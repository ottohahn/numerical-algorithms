#!/usr/bin/env python
"""
Generates the coefficients of a polynomial of the given degree
that passes through the given points in an array
"""
import numpy as np

def polynomial_interpolation_2d(xy_pairs, degree, x_interpolate):
    """
    Performs polynomial interpolation using 2D numpy array of x, y pairs and evaluates the polynomial at a given point.
    
    Parameters:
    xy_pairs (numpy.ndarray): 2D array of shape (n, 2) containing x, y pairs of data points.
    degree (int): Degree of the polynomial to fit.
    x_interpolate (float): x-coordinate for which to interpolate the y-coordinate.
    
    Returns:
    float: Interpolated y-coordinate.
    """
    x_points = xy_pairs[:, 0]
    y_points = xy_pairs[:, 1]
    
    coefficients = np.polyfit(x_points, y_points, degree)
    interpolated_y = np.polyval(coefficients, x_interpolate)
    
    return interpolated_y
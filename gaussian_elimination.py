import numpy as np

def gaussian_elimination(A, b):
    """
    Performs Gaussian elimination on a system of linear equations represented by matrix A and vector b.
    
    Parameters:
    A (numpy.ndarray): Coefficient matrix.
    b (numpy.ndarray): Right-hand side vector.
    
    Returns:
    numpy.ndarray: Solution vector.
    """
    n = A.shape[0]
    
    # Augment the matrix A with vector b
    augmented_matrix = np.hstack((A, b.reshape(-1, 1)))
    
    # Forward elimination
    for i in range(n):
        pivot_row = augmented_matrix[i]
        pivot = pivot_row[i]
        
        # Divide the pivot row by the pivot element
        augmented_matrix[i] = pivot_row / pivot
        
        # Eliminate other rows
        for j in range(i + 1, n):
            factor = augmented_matrix[j, i]
            augmented_matrix[j] -= factor * augmented_matrix[i]
    
    # Backward substitution
    x = np.zeros(n)
    for i in range(n - 1, -1, -1):
        x[i] = augmented_matrix[i, -1] - np.dot(augmented_matrix[i, i+1:n], x[i+1:n])
    
    return x
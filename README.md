# Numerical Methods

## Purpose

The purpose of this repository is to showcase a collection of numerical methods taken from several sources. These methods have been implemented in Python using libraries such as Numpy and Pandas, as well as base data structures. The goal is to have a handy reference for these numerical techniques in case they are needed for various computational tasks.

## Algorithms

This repository contains implementations of the following numerical methods:

1. **Calculation of e**
    - Description: Approximating the mathematical constant 'e' using various numerical techniques.
    
2. **Calculation of pi using Archimedes' method**
    - Description: Estimating the value of pi (π) through Archimedes' geometric approach.
    
3. **Calculation of pi using Machin's method**
    - Description: Computing pi (π) using John Machin's formula.
    
4. **Calculation of the square root of 2**
    - Description: Finding an approximation of the square root of 2 using numerical methods.
    
5. **Park-Miller random number generator**
    - Description: Implementing the Park-Miller random number generator for generating pseudo-random numbers.
    
6. **Linear interpolation**
    - Description: Interpolating values using linear interpolation techniques between two data points.
    
7. **Polynomial interpolation**
    - Description: Performing polynomial interpolation to approximate functions based on data points.
    
8. **Gauss elimination**
    - Description: Implementing the Gauss elimination method to solve systems of linear equations.
    
9. **Newton-Raphson root finding**
    - Description: Using the Newton-Raphson method to find roots of equations numerically.
    
10. **Finite differences numerical differentiation**
    - Description: Calculating numerical derivatives using finite differences for one-dimensional functions.

11. **Trapezoidal rule integration (Newton-Cotes)**
    - Description: Integrate a one-dimensional variable using the trapezoidal rule.

Each algorithm is implemented as a separate module, making it easy to understand and use in various numerical computing tasks. The code is written in Python for simplicity and readability.

Feel free to explore each algorithm's implementation and adapt them for your specific needs or as a reference for understanding numerical methods.

## Usage

To use any of the numerical methods implemented in this repository, simply import the relevant module and call the function.

## Contributions

Contributions to this repository are welcome! If you have additional numerical methods or improvements to existing ones, please consider opening a pull request.

## License

This repository is licensed under the [WTFPL - Do What The F*ck You Want To Public License](http://www.wtfpl.net/about/).

---

**Note**: This repository is for educational purposes and as a reference for numerical methods. It may not cover all edge cases or be optimized for production use.


#!/usr/bin/env python
"""
Random number generator in a uniform distribution using the recipe from:
    Numerical Recipes in FORTRAN 77 pp 269.
"""
import time
import math

idum = 3453

def ran0(seed):
    """
    This is the Park-Miller generator
    here we use the time to seed the generator
    """
    global idum
    a = 16807
    m = 2147483647
    rand = ((a * idum) % m)
    idum = rand
    return rand / m

for i in range(0, 100):
    print(idum, ran0(idum))


#!/usr/bin/env python
"""
Calculates the linear interpolation between two points
"""

def linear_interpolation(x1, y1, x2, y2):
    """
    Calculates linear interpolation between two points.
    
    Parameters:
    x1 (float): x-coordinate of the first point.
    y1 (float): y-coordinate of the first point.
    x2 (float): x-coordinate of the second point.
    y2 (float): y-coordinate of the second point.
    x (float): x-coordinate for which to interpolate the y-coordinate.
    
    Returns:
    float: Interpolated y-coordinate.
    """
    if x1 == x2:
        raise ValueError("x1 and x2 cannot be equal.")
    
    y = y1 + (y2 - y1) * (x - x1) / (x2 - x1)
    return y

#!/usr/bin/env python
"""
Program to calculate square roots using Goldschmidt's algorithm
"""
import math

def goldschmidt_sqrt(x, iterations=10):
    # Initialize constants
    c = 1.0
    y = x / 2
    # Perform iterative calculations
    for i in range(iterations):
        c = c * 0.5
        y = (y + x / y) * 0.5

    # Return the final result
    # return y / c
    return y

# Calculate the square root of 2
result = goldschmidt_sqrt(2, 12)
print(result)
print(math.sqrt(2))
print(result-math.sqrt(2))

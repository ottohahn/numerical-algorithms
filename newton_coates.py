import numpy as np
from scipy.integrate import trapz

# Define the function to be integrated
def f(x):
    return x**2

def integrate(f, a, b):
    """
    Define the integration interval [a, b]
    and function f(x) to be integrated
    """
    # Generate equally spaced points within the interval
    x = np.linspace(a, b, 100)
    # Calculate the function values at those points
    y = f(x)
    # Use the Trapezoidal Rule to perform numerical integration
    result = trapz(y, x)
    return result

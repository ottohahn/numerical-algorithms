def newton_raphson(f, df, x0, tol=1e-6, max_iter=100):
    """
    Performs the Newton-Raphson method to find a root of the equation f(x) = 0.
    
    Parameters:
    f (function): The function for which to find a root.
    df (function): The derivative of the function f.
    x0 (float): Initial guess for the root.
    tol (float): Tolerance for convergence.
    max_iter (int): Maximum number of iterations.
    
    Returns:
    float: Approximate root of the equation f(x) = 0.
    """
    x = x0
    
    for _ in range(max_iter):
        x_new = x - f(x) / df(x)
        
        if abs(x_new - x) < tol:
            return x_new
        
        x = x_new
    
    raise RuntimeError("Newton-Raphson method did not converge.")

# Example usage
def func(x):
    return x**3 - 5

def func_derivative(x):
    return 3 * x**2

initial_guess = 2.0
approx_root = newton_raphson(func, func_derivative, initial_guess)
print("Approximate root:", approx_root)

"""
pi-calcs is a module for calculating pi using different approximation algorithms
mainly fractions and  Machin-like formulas.

"""
import math
import decimal as dc


def calc_exhaustion(sides):
    """
    Exhaustion method of archimedes
    """
    if sides < 6:
        print("Error sides less than 6")
        return -1
    else:
        if ((sides % 6 == 0) and (sides % 2 == 0)):
            # do stuff
            o_pi = dc.Decimal(0.0)
            side = dc.Decimal(6.)
            co_sq = dc.Decimal(0.25)
            sn_sq = dc.Decimal(0.75)
            d_sides = dc.Decimal(sides)

            while (side <= d_sides):
                peri = dc.sqrt(co_sq) * side
                o_pi = (peri * dc.sqrt(sn_sq)) / dc.Decimal(2.)
                # print(side, 360./side,co_sq,sn_sq,peri,o_pi)
                co_sq = (dc.Decimal(1.) + dc.sqrt(co_sq)) / dc.Decimal(2.)
                sn_sq = dc.Decimal(1.) - co_sq
                side *= dc.Decimal(2.)
            return o_pi
        else:
            print("Error sides is not a multiple of 6 and 2") 
            return -1
    
def machin(options):
    if options != 0:
        return (16 * math.atan(0.2)) - (4 * math.atan( 1 / 239))

def calc_pi(method, options=0):
    if method == 'babylonian':
        return 25. / 8.
    elif method == 'egyptian':
        return 22. / 7.
    elif method == 'rhind':
        return 256. / 81.
    elif method == 'exhaustion':
       if options == 0:
            return "Error, sides must be larger than 6"
       else:     
            return calc_exhaustion(options)
    elif method == 'fractions':
        if options == 0:
            return "Error, invalid option"
        if options == 1:
            return 333. / 106.
        elif options == 2:
            return 355. / 113.
        elif options == 3:
            return 103993. / 33102.
        elif options == 4:
            return 104348. / 33215.
        elif options == 5:
            return 208341. / 66317.
        elif options == 6:
            return 312689. / 99532.
        elif options == 7:
            return 833719. / 265381.
        elif options == 8:
            return 1146408. / 364913.
    elif method == 'machin':
        if options == 0:
            return 'Error, invalid option'
        else:
            return machin(options)
            

if __name__ == '__main__':
    #print(calc_pi('babylonian'))
    #print(calc_pi('egyptian'))
    #print(calc_pi('rhind'))
    #for i in range(0, 9):
    #    print(calc_pi('fractions', i))
    print(calc_pi('machin', 1))   

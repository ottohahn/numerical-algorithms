import numpy as np
from scipy.optimize import approx_fprime

def finite_differences_derivative(f, x, h, method="forward"):
    """
    Calculate the finite differences derivative of a function y = f(x).
    
    Parameters:
    f (function): The function for which to calculate the derivative.
    x (float or numpy.ndarray): The point(s) at which to calculate the derivative.
    h (float): Step size for finite differences.
    method (str): The finite differences method to use ("forward" or "backward").
    
    Returns:
    float or numpy.ndarray: Approximate derivative(s).
    """
    if method not in ["forward", "backward"]:
        raise ValueError("Method should be 'forward' or 'backward'.")
    
    if method == "forward":
        if isinstance(x, (int, float)):
            x_values = np.array([x])
        else:
            x_values = np.array(x)
            
        derivatives = np.zeros_like(x_values, dtype=float)
        for i, x_val in enumerate(x_values):
            def func_to_approx(x):
                return f(x_val + x)
            
            derivative = approx_fprime(0.0, func_to_approx, h)
            derivatives[i] = derivative
        
        return derivatives[0] if len(derivatives) == 1 else derivatives
    
    elif method == "backward":
        if isinstance(x, (int, float)):
            x_values = np.array([x])
        else:
            x_values = np.array(x)
            
        derivatives = np.zeros_like(x_values, dtype=float)
        for i, x_val in enumerate(x_values):
            def func_to_approx(x):
                return f(x_val - x)
            
            derivative = approx_fprime(0.0, func_to_approx, h)
            derivatives[i] = derivative
        
        return derivatives[0] if len(derivatives) == 1 else derivatives

#!/usr/bin/env python
"""
Calculates Euler's number e to a desired precision
using different algorithms
"""
from decimal import Decimal, getcontext

def compute_e(n):
    """
    Calculates the numerical value of e to n number of digits.
    """
    getcontext().prec = n + 1
    e0 = Decimal(0)
    e = Decimal(2)
    k = Decimal(0)
    fact = Decimal(1)
    err_n = Decimal(n)
    error = Decimal(1/(10**err_n))
    count = 0
    while(e-e0 > error):
        e0 = e
        count += 1
        k += 1
        fact *= Decimal(2)*k*(Decimal(2)*k+Decimal(1))
        e += (Decimal(2)*k+Decimal(2))/fact
        print(count, e)
    return e

e = compute_e(36)
print(e)
print(e-Decimal(2.7182818284590452353602874713526624))

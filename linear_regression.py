import numpy as np

def linear_regression(x, y):
    """
    Performs linear regression to find the best-fitting line through the data points.
    
    Parameters:
    x (numpy.ndarray): Independent variable values.
    y (numpy.ndarray): Dependent variable values.
    
    Returns:
    tuple: Slope (m) and intercept (b) of the regression line.
    """
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    
    # Calculate the slope (m) and intercept (b) of the regression line
    numerator = np.sum((x - x_mean) * (y - y_mean))
    denominator = np.sum((x - x_mean) ** 2)
    slope = numerator / denominator
    intercept = y_mean - slope * x_mean
    
    return (slope, intercept)